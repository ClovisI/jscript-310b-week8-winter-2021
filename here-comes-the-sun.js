const body = document.getElementsByTagName('body')[0];
let colorValue = 0;


function sunLight(){
  colorValue+=1;
  body.style.backgroundColor =`rgb(${colorValue},${colorValue},${colorValue})`;
  if(colorValue<255){

    requestAnimationFrame(sunLight);
  }
}
requestAnimationFrame(sunLight);
