describe('Soccer game, testing getTotalPoints function', function() {
  it("This should return the correct point total", function() {
    const points = getTotalPoints('wwdl');
    expect(points).toBe(7);
    expect(getTotalPoints('dddddl')).toBe(5);
    expect(getTotalPoints('wwwwdlll')).toBe(13);
  });

  it("This should return proper points with corresponding team", function(){
    const team1 = {
      name: 'Sounders',
      results: 'wwwwwldl'
    };
    const team2 = {
      name: 'Galaxy',
      results: 'ldwdwlwl'
    };

    const expected = `Sounders: 16
Galaxy: 11`;

    expect(orderTeams(team1, team2)).toEqual(expected);
  });
});
