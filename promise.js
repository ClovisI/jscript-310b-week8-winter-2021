let myPromise = new Promise(function(resolve, reject) {
  setTimeout(function() {
    if(Math.random() >= .5){
      resolve();
    } else{
      reject();
    }
  }, 1000);
});

myPromise
  .then(function() {
    console.log('success');
  })
  .catch(function number(){
    console.log('fail');
  })
  .then(function() {
    console.log('complete');
  });
