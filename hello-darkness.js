const body = document.getElementsByTagName('body')[0];
let colorValue = 255;
body.style.backgroundColor = 'rgb(255, 255, 255)';

setInterval(function(){
  if(colorValue>1){
    colorValue-=1;
    body.style.backgroundColor =`rgb(${colorValue},${colorValue},${colorValue})`;
  }
}, 500);
