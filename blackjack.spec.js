describe("Test cases for validating the dealerShouldDraw function; true means draw, false means hold", function(){
    const ten = {val: 11, displayVal: '10', suit: 'spades'};
    const nine = {val: 9, displayVal: '9', suit: 'spades'};
    const six =  {val: 6, displayVal: '6', suit: 'spades'};
    const five = {val: 5, displayVal: '5', suit: 'spades'};
    const four = {val: 4, displayVal: '4', suit: 'spades'};
    const two = {val: 2, displayVal: '2', suit: 'spades'};
    const Ace = {val: 11, displayVal: 'Ace', suit: 'spades'};

    it("If dealer has 10 + 9 = 19 they should hold", function() {
        expect(dealerShouldDraw([ten,nine])).toEqual(false);
    });

    it("If dealer has Ace + 6 = 17 they should draw", function() {
        expect(dealerShouldDraw([Ace,six])).toEqual(true);
    });

    it("If dealer has 10 + 6 + Ace = 17 they should hold", function() {
        expect(dealerShouldDraw([ten,six,Ace])).toEqual(false);
    });

    it("If dealer has 2 + 4 + 2 + 5 = 13 they should draw", function() {
        expect(dealerShouldDraw([two,four,two,five])).toEqual(true);
    });
});
